================================================================================
python template
================================================================================
--------------------------------------------------------------------------------
README
--------------------------------------------------------------------------------
:Info: Info ...
:Author: Name Here <name.here@example.com>
:Date: |date|
:Revision: 000
:Description: Description ...

.. |date| date:: %Y-%m-%d
.. |time| date:: %H:%M
.. sectnum::
.. contents::
    :depth: 5

Dev stuff
================================================================================

Using
--------------------------------------------------------------------------------

Files to modify/exclude

.. code:: bash

    ./README.rst
    ./setup.cfg
    ./setup.py
    ./src/iwana/**
    ./tests/test_template.py

...

.. code:: bash

    rsync -av --no-owner --no-group --no-times --checksum --no-perms --ignore-existing \
        --exclude='*.pyc' --exclude='__pycache__' --exclude='.pytest_cache' \
        --exclude='.eggs' --exclude '*.egg-info' \
        --exclude='.git' --exclude='README.*' \
        ./ user/

    find . \( -regex '^.*/.git$' \) -prune -o -type f -print \
        | xargs -n1 -t sed -E -n \
        -e 's|iwana([/.])template|xadix\1argparse_tree|gp' \
        -e 's|iwana|xadix|gp'

    find . \( -regex '^.*/.git$' \) -prune -o -type f -print \
        | xargs -n1 -t sed -E -i \
        -e 's|iwana([/.])template|xadix\1argparse_tree|g' \
        -e 's|iwana|xadix|g'

    diff -u -x '.*' -x '*.pyc' -x 'README.*' -x src -x test -x '*.egg-info' -r ./ user/

    # ... | egrep '^(Only|diff)'

    git clean -dn
    git clean -df

Running and Editing
--------------------------------------------------------------------------------

Python 3

Install editable:

.. code:: bash

    pip3 uninstall iwana.template
    pip3 install --user --upgrade --editable .
    python3 -m iwana.template.cli -vvvv --version

Run without installing:

.. code:: bash

    python3 -m src.iwana.template.cli -vvvv --version
    python3 -m src.iwana.template.cli -vvvv

Other:

.. code:: bash

    git clean -dn
    git clean -df

Python 2

.. code:: bash

    pip2 install --user --upgrade --editable .
    pip2 uninstall iwana-toolkit
    python2 -m iwana.toolkit.cli -vvvv --version


PyTest
--------------------------------------------------------------------------------

* https://docs.pytest.org/en/latest/goodpractices.html
* https://docs.pytest.org/en/latest/projects.html

.. code:: bash

    pip3 install --user --upgrade pytest pytest-cov
    python3 setup.py test

    python3 -m pytest tests
    pytest tests
    py.test tests

.. note:: Running pytest
    Pytest does not pick up modules directly, needs PYTHONPATH=src
    Not sure why. Need to figure out.


Python
================================================================================

.. code:: bash

    python3 -c 'import setuptools; help(setuptools.find_namespace_packages)' | cat
    pydoc3 setuptools
    pydoc3 setuptools.find_namespace_packages

* https://docs.python.org/3/glossary.html#term-module
* https://docs.python.org/3/glossary.html#term-package
* https://docs.python.org/3/reference/datamodel.html
* https://docs.python.org/3/library/types.html

ReStructuredText
================================================================================

Info on ReStructuredText

* reStructuredText_
* `A ReStructuredText Primer`_
* `Quick reStructuredText <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`_
* `The reStructuredText Cheat Sheet: Syntax Reminders <http://docutils.sourceforge.net/docs/user/rst/cheatsheet.txt>`_
* `reStructuredText Directives <http://docutils.sourceforge.net/docs/ref/rst/directives.html>`_

.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _A ReStructuredText Primer: http://docutils.sourceforge.net/docs/user/rst/quickstart.html

Viewing rst
--------------------------------------------------------------------------------

.. code:: bash

    pip3 install --user --upgrade --ignore-installed restview
    restview README.rst

Styling
================================================================================

`Vim <https://www.vim.org/>`_ modeline:

.. code:: vim

    # vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=79 cc=+1:
    # vim: set filetype=python sts=4 ts=4 sw=4 expandtab:

`EditorConfig <https://editorconfig.org/>`_ ``.editorconfig`` file:

.. code:: ini

    # http://editorconfig.org/#file-format-details
    root = true

    [*]
    indent_style = space
    indent_size = 4
    tab_width = 4
    end_of_line = lf
    charset = utf-8
    insert_final_newline = true
    trim_trailing_whitespace = true

    [{*.{mk,make},makefile,Makefile}]
    indent_style = tab
    indent_size = 4
    tab_width = 4
    end_of_line = lf
    charset = utf-8
    insert_final_newline = true
    trim_trailing_whitespace = true


* `PEP 8 -- Style Guide for Python Code <https://www.python.org/dev/peps/pep-0008/>`_
* `Google Python Style Guide <http://google.github.io/styleguide/pyguide.html>`_

Documentation
================================================================================

* `PEP 257 -- Docstring Conventions <https://www.python.org/dev/peps/pep-0257/>`_
* https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html
* https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#cross-referencing-python-objects
* https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
* https://www.sphinx-doc.org/en/master/usage/advanced/setuptools.html
* https://realpython.com/documenting-python-code/
* https://github.com/google/styleguide/blob/gh-pages/pyguide.md#38-comments-and-docstrings
* https://sphinxcontrib-napoleon.readthedocs.io/en/latest/
* http://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings
* https://stackoverflow.com/questions/3898572/what-is-the-standard-python-docstring-format
* https://stackoverflow.com/questions/54189661/docstring-in-class-or-init-constructor
* https://stackoverflow.com/questions/5599254/how-to-use-sphinxs-autodoc-to-document-a-classs-init-self-method

.. code:: bash

    pip3 install --user sphinx sphinxcontrib-napoleon

    sphinx-quickstart --project '' --author '' -v '' --release '' --language en --sep \
        --ext-autodoc --ext-doctest --ext-intersphinx --ext-todo --ext-coverage \
        --ext-imgmath --ext-mathjax --ext-ifconfig --ext-viewcode --ext-githubpages \
        docs
    sphinx-apidoc --force \
        --ext-autodoc --ext-doctest --ext-intersphinx --ext-todo --ext-coverage \
        --ext-imgmath --ext-mathjax --ext-ifconfig --ext-viewcode --ext-githubpages \
        --output-dir docs/source/ src/iwana/

    make -C docs/ html


        --ext-autodoc --ext-doctest --ext-intersphinx --ext-todo --ext-coverage \
        --ext-imgmath --ext-mathjax --ext-ifconfig --ext-viewcode --ext-githubpages \

Example Projects
================================================================================

Sample
--------------------------------------------------------------------------------

* https://github.com/pypa/sampleproject
* https://github.com/kennethreitz/samplemod
* https://pythonhosted.org/an_example_pypi_project/index.html
* https://github.com/trammell/an_example_pypi_project

Template
--------------------------------------------------------------------------------

* https://github.com/audreyr/cookiecutter-pypackage

Real
--------------------------------------------------------------------------------

* https://github.com/rg3/youtube-dl
* https://github.com/wireservice/csvkit
* https://github.com/conan-io/conan
* https://github.com/sivel/speedtest-cli
* https://github.com/audreyr/cookiecutter
* https://github.com/AnalogJ/lexicon
* https://github.com/kennethreitz/requests
* https://github.com/pwman3/pwman3
* https://github.com/Azure/azure-storage-python
* https://github.com/Julian/jsonschema
* https://github.com/jmespath/jmespath.py
* https://opendev.org/openstack/pbr/
* https://github.com/giampaolo/psutil
* https://github.com/pallets/itsdangerous
* https://github.com/matplotlib/matplotlib
* https://github.com/pyca/pynacl

Lists
--------------------------------------------------------------------------------

* https://pythonwheels.com/
* https://libraries.io/pypi
* http://py3readiness.org/
* https://wiki.python.org/moin/UsefulModules

Packaging
================================================================================

Guides
--------------------------------------------------------------------------------

Official
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* https://packaging.python.org/
* https://packaging.python.org/tutorials/packaging-projects/
* https://packaging.python.org/guides/distributing-packages-using-setuptools/


* https://www.python.org/dev/peps/pep-0008/
* https://www.python.org/dev/peps/pep-0561/

* https://wiki.python.org/moin/Distutils/Tutorial

Other
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* https://docs.python-guide.org/writing/structure/

* https://cookiecutter.readthedocs.io/en/latest/
* https://realpython.com/python-application-layouts/

* http://the-hitchhikers-guide-to-packaging.readthedocs.io/en/latest/quickstart.html
* http://python-packaging.readthedocs.io/en/latest/command-line-scripts.html

* http://learnpythonthehardway.org/book/ex46.html

Namespaces
--------------------------------------------------------------------------------

* https://www.python.org/dev/peps/pep-0420/
* https://setuptools.readthedocs.io/en/latest/pkg_resources.html
* https://docs.python.org/2/library/pkgutil.html
* https://docs.python.org/3/library/pkgutil.html
* https://packaging.python.org/guides/packaging-namespace-packages/
* https://setuptools.readthedocs.io/en/latest/pkg_resources.html#namespace-package-support
* https://setuptools.readthedocs.io/en/latest/setuptools.html#namespace-packages

.. code:: python

    ## RECOMMENDED
    ## pkgutil
    # https://docs.python.org/2/library/pkgutil.html
    # https://docs.python.org/3/library/pkgutil.html
    __path__ = __import__('pkgutil').extend_path(__path__, __name__)


    ## pkg_resources
    # https://setuptools.readthedocs.io/en/latest/pkg_resources.html
    __import__('pkg_resources').declare_namespace(__name__)

Distutils
--------------------------------------------------------------------------------

Python 2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* https://docs.python.org/2/distutils/sourcedist.html
* https://docs.python.org/2/distutils/configfile.html
* https://docs.python.org/2/distutils/setupscript.html#additional-meta-data


Python 3
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* https://docs.python.org/3/distutils/sourcedist.html
* https://docs.python.org/3/distutils/introduction.html#distutils-simple-example
* https://docs.python.org/3/distutils/setupscript.html#additional-meta-data


Setuptools
--------------------------------------------------------------------------------

* https://setuptools.readthedocs.io/en/latest/
* https://setuptools.readthedocs.io/en/latest/setuptools.html#new-and-changed-setup-keywords
* https://pypi.org/project/setuptools/
* https://github.com/pypa/setuptools

Other
--------------------------------------------------------------------------------

* https://packaging.python.org/discussions/install-requires-vs-requirements/
* https://pip.pypa.io/en/latest/user_guide/


Dist
================================================================================

* https://pypi.org/help/#description-content-type
* https://packaging.python.org/tutorials/packaging-projects/#description
* https://github.com/pypa/readme_renderer

.. code:: bash

    pip3 install --user --upgrade wheel twine

    git tag --message="tag version" 0.1.0
    python3 setup.py clean --all
    rm -rf dist/ *.egg-info

    python3 setup.py bdist_wheel
    twine upload dist/*
    twine check


Template tools
================================================================================

Cookiecutter
--------------------------------------------------------------------------------

* https://cookiecutter.readthedocs.io
* https://github.com/audreyr/cookiecutter
* https://pypi.python.org/pypi/cookiecutter
* https://cookiecutter.readthedocs.io/en/latest/usage.html

.. code:: bash

    pip3 install --user --upgrade --ignore-installed cookiecutter
    rm -rf /var/tmp/cookiecutter_example
    # ...

PyScaffold
--------------------------------------------------------------------------------

* https://pyscaffold.org/en/latest/
* https://github.com/pyscaffold/pyscaffold/
* https://pypi.org/project/PyScaffold/

.. code:: bash

    pip3 install --user --upgrade --ignore-installed pyscaffold
    rm -rf /var/tmp/pyscaffold_example
    putup \
        --gitlab --tox --pre-commit \
        --package pyscaffold_example \
        --description 'PyScaffold Example' \
        --namespace nmspex \
        --url https://pyscaffold.example.com/ \
        /var/tmp/pyscaffold_example


Versioning
================================================================================

Versioneer
--------------------------------------------------------------------------------

* https://github.com/warner/python-versioneer
* https://pypi.org/project/versioneer/
* https://github.com/warner/python-versioneer/blob/master/INSTALL.md

.. code:: bash

    pip3 install --user --upgrade versioneer
    versioneer install

Other
================================================================================

``MANIFEST.in``

* https://docs.python.org/3/distutils/sourcedist.html
* https://docs.python.org/3/distutils/commandref.html#sdist-cmd

* https://docs.python.org/2/distutils/sourcedist.html

