#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:
# pylint: disable=missing-docstring

class MyClass:
    """
    Some class
    """
    # pylint: disable=too-few-public-methods
    #   ...
    def __init__(self):
        """
        Constructor for the class ...
        """
        self.something = "somevalue"

    def method(self):
        return self.something
