#!/usr/bin/env python3
# vim: set tw=100 cc=+1:
"""
Command line interface module
"""

# pylint: disable=missing-docstring
# pylint: disable=bad-continuation
# pylint: disable=too-many-locals
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-few-public-methods
import logging
import argparse
import sys
import types

import coloredlogs

from . import __version__

logger = logging.getLogger(__name__) # pylint: disable=invalid-name
class Main:
    """
    Class for CLI Main
    """

    def __init__(self):
        self.apn_root = types.SimpleNamespace(parser=argparse.ArgumentParser(add_help=True))
        self.parse_result = None

    def exec(self):
        """
        Exec function for CLI Main
        """
        apn_current = apn_root = self.apn_root

        apn_current.parser.add_argument("--version",
            action="version", version="{:s}".format(__version__))
        apn_current.parser.add_argument("-v", "--verbose",
            action="count", dest="verbosity",
            help="increase verbosity level")

        parse_result = self.parse_result = apn_root.parser.parse_args(args=sys.argv[1:])
        verbosity = parse_result.verbosity
        if verbosity is not None:
            root_logger = logging.getLogger("")
            root_logger.propagate = True
            new_level = (root_logger.getEffectiveLevel() -
                (min(1, verbosity)) * 10 - min(max(0, verbosity - 1), 9) * 1)
            root_logger.setLevel(new_level)

        logger.debug("sys.argv = %s, parse_result = %s, logging.level = %s, logger.level = %s",
            sys.argv, parse_result, logging.getLogger("").getEffectiveLevel(),
            logger.getEffectiveLevel())

def exec_main():
    '''
    logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%dT%H:%M:%S', stream=sys.stderr,
        format=("%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"))
    '''

    root_logger = logging.getLogger("")
    root_logger.propagate = True
    root_logger.setLevel(logging.INFO)
    handler = logging.StreamHandler(sys.stderr)
    handler.setFormatter(coloredlogs.ColoredFormatter(datefmt="%Y-%m-%dT%H:%M:%S",
        fmt=("%(asctime)s %(process)d %(thread)x %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s")
        ))
    root_logger.addHandler(handler)
    main = Main()
    main.exec()

if __name__ == "__main__":
    exec_main()
