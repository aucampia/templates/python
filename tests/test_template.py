#!/usr/bin/env python3
# vim: set tw=100 cc=+1:
# pylint: disable=missing-docstring

# pylint: disable=unused-import

import pytest
import iwana.template
import iwana.template.bohsh

def test_foo():
    assert 1 != 2

def test_foof():
    assert iwana.template.some_function("foo") == "some_function:some_string:foo"

def test_fooc():
    assert iwana.template.MyClass().something == "somevalue"
    assert iwana.template.MyClass().method() == "somevalue"

def test_food():
    assert iwana.template.bohsh.BOHSH_VALUE == "valuevalue"
